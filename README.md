# Lawgit

Lawgit is a logging library developed to create an easy logging experience. Many librarys have unnecessary features for our use, leading into a complex setup and unfortunate breaking API changes. This is our attempt to simplify our logging process.

## Installation

```
npm install lawgit
```


## Usage

Example simple usage:

```js
const Lawgit = require('lawgit').default;
const logger = Lawgit();

logger.log('Hello!');
```

## API

In the example above, `Lawgit()` is a factory function that returns a few functions:

| Function            | Arguments   | Description                                             |
| -----------------   | ----------- | ------------------------------------------------------- |
| globalSettings      | none        | Returns the global settings object                      |
| setGlobalSettings   | Settings    | Overrides the global settings object with new settings  |
| resetGlobalSettings | none        | Sets the global settings object to the original object at initial runtime |
| log                 | variable strings | Logs the message out using the default message type and color|
| logWithSettings     | Settings, variable strings | Logs the message out using the overriding the default message type and color with Settings |
| debug               | variable strings | Logs the message out using the debug message type and default color |
| error               | variable strings | Logs the message out using the error message type and default color |
| info                | variable strings | Logs the message out using the info message type and default color |
| warn                | variable strings | Logs the message out using the warn message type and default color |

The Settings object is created with this interface:

```ts
interface Settings {
  color: boolean;     // if true, message is displayed in the color for the given level
  level: LEVEL;       // one of: 'debug', 'info', 'warn', or 'error'
}
```
