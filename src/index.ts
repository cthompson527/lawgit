import colors from './colors';
import levels from './levels';

type LEVEL = 'debug' | 'info' | 'warn' | 'error';

interface Settings {
  color: boolean;
  level: LEVEL;
  enabledLevels: number;
}

const defaultGlobals: Settings = {
  color: true,
  level: 'info',
  enabledLevels: levels.debug | levels.info | levels.warn | levels.error,
};

let globalSettings = { ...defaultGlobals };

const colorsByLevel = {
  info: colors.fg.green,
  warn: colors.fg.yellow,
  debug: colors.fg.blue,
  error: colors.fg.red,
};

function setGlobalSettings(newSettings: Partial<Settings>) {
  globalSettings = {
    ...globalSettings,
    ...newSettings,
  };
}

function resetGlobalSettings() {
  setGlobalSettings(defaultGlobals);
}

function logWithSettings(settings: Partial<Settings>, ...msg: string[]) {
  const combinedSettings = { ...globalSettings, ...settings };
  const { level, color } = combinedSettings;
  const skipLog = !(combinedSettings.enabledLevels & levels[level]);
  if (skipLog) {
    return;
  }
  if (color) {
    console[level](`${colorsByLevel[level]}${msg.join(' ')}${colors.reset}`);
  } else {
    console[level](...msg);
  }
}

function logit() {
  return {
    globalSettings: (): Settings => globalSettings,
    setGlobalSettings,
    resetGlobalSettings,
    log: (...msg: string[]) => logWithSettings({}, ...msg),
    logWithSettings,
    debug: (...msg: string[]) => logWithSettings({ level: 'debug' }, ...msg),
    error: (...msg: string[]) => logWithSettings({ level: 'error' }, ...msg),
    warn: (...msg: string[]) => logWithSettings({ level: 'warn' }, ...msg),
    info: (...msg: string[]) => logWithSettings({ level: 'info' }, ...msg),
    levels,
  };
}

export default logit;
