export default {
  debug: 1 << 3,
  info: 1 << 2,
  warn: 1 << 1,
  error: 1,
  all: 0b1111,
  none: 0b0000,
};
