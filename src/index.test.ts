import Lawgit from '.';
import colors from './colors';
import levels from './levels';

test('global settings are set with defaults', () => {
  const logger = Lawgit();

  expect(logger.globalSettings()).toEqual({
    color: true,
    level: 'info',
    enabledLevels: levels.all,
  });
});

test('global settings are manipulated with the setGlobalSettings fn', () => {
  const logger = Lawgit();
  const otherLogger = Lawgit();

  logger.setGlobalSettings({});
  expect(otherLogger.globalSettings()).toEqual({
    color: true,
    level: 'info',
    enabledLevels: levels.all,
  });

  logger.setGlobalSettings({ color: false });
  expect(otherLogger.globalSettings()).toEqual({
    color: false,
    level: 'info',
    enabledLevels: levels.all,
  });

  logger.setGlobalSettings({ level: 'debug' });
  expect(otherLogger.globalSettings()).toEqual({
    color: false,
    level: 'debug',
    enabledLevels: levels.all,
  });
});

describe('console logging tests', () => {
  let spyDebug: any, spyInfo: any, spyWarn: any, spyError: any;

  beforeAll(() => {
    spyDebug = jest.spyOn(console, 'debug').mockImplementation();
    spyError = jest.spyOn(console, 'error').mockImplementation();
    spyInfo = jest.spyOn(console, 'info').mockImplementation();
    spyWarn = jest.spyOn(console, 'warn').mockImplementation();
  });

  beforeEach(Lawgit().resetGlobalSettings);
  afterEach(jest.clearAllMocks);
  afterAll(jest.resetAllMocks);

  test('mocks are working correctly', () => {
    console.debug('debug');
    console.info('info');
    console.warn('warn');
    console.error('error');
    expect(spyDebug).toHaveBeenCalledWith('debug');
    expect(spyInfo).toHaveBeenCalledWith('info');
    expect(spyWarn).toHaveBeenCalledWith('warn');
    expect(spyError).toHaveBeenCalledWith('error');
  });

  test('logging info logs green when colors are enabled', () => {
    const logger = Lawgit();
    logger.log('Test!');
    expect(spyInfo).toHaveBeenCalledWith(
      `${colors.fg.green}Test!${colors.reset}`,
    );
  });

  test('logging info logs without color when colors are disabled', () => {
    const logger = Lawgit();
    logger.setGlobalSettings({ color: false });
    logger.log('Test!');
    expect(spyInfo).toHaveBeenCalledWith('Test!');
  });

  test('logging debug logs blue when colors are enabled', () => {
    const logger = Lawgit();
    logger.setGlobalSettings({ level: 'debug' });
    logger.log('Test!');
    expect(spyDebug).toHaveBeenCalledWith(
      `${colors.fg.blue}Test!${colors.reset}`,
    );
  });

  test('loggingWithSettings uses custom settings to override the global', () => {
    const logger = Lawgit();
    logger.logWithSettings({ level: 'error' }, 'Test!');
    expect(spyError).toHaveBeenCalledWith(
      `${colors.fg.red}Test!${colors.reset}`,
    );

    logger.logWithSettings({ level: 'warn' }, 'Test!');
    expect(spyWarn).toHaveBeenCalledWith(
      `${colors.fg.yellow}Test!${colors.reset}`,
    );

    logger.logWithSettings({ level: 'debug', color: false }, 'Test!');
    expect(spyDebug).toHaveBeenCalledWith('Test!');
  });

  test('debug fn uses debug message type and color', () => {
    const logger = Lawgit();
    logger.debug('Test!');
    expect(spyDebug).toHaveBeenCalledWith(
      `${colors.fg.blue}Test!${colors.reset}`,
    );
  });

  test('info fn uses debug message type and color', () => {
    const logger = Lawgit();
    logger.info('Test!');
    expect(spyInfo).toHaveBeenCalledWith(
      `${colors.fg.green}Test!${colors.reset}`,
    );
  });

  test('warn fn uses debug message type and color', () => {
    const logger = Lawgit();
    logger.warn('Test!');
    expect(spyWarn).toHaveBeenCalledWith(
      `${colors.fg.yellow}Test!${colors.reset}`,
    );
  });

  test('error fn uses debug message type and color', () => {
    const logger = Lawgit();
    logger.error('Test!');
    expect(spyError).toHaveBeenCalledWith(
      `${colors.fg.red}Test!${colors.reset}`,
    );
  });

  test('debug logger is not called when debug logging is not on', () => {
    const logger = Lawgit();
    logger.debug('Test!');
    expect(spyDebug).toHaveBeenCalledWith(
      `${colors.fg.blue}Test!${colors.reset}`,
    );

    spyDebug.mockClear();
    logger.setGlobalSettings({
      enabledLevels: levels.error | levels.info | levels.warn,
    });
    logger.debug('Test!');
    expect(spyDebug).not.toHaveBeenCalled();
  });

  test('logger can temporarily logWithSettings when global settings are turned off', () => {
    const logger = Lawgit();
    logger.setGlobalSettings({ enabledLevels: levels.none });
    logger.info('Test!');
    expect(spyInfo).not.toHaveBeenCalled();

    logger.logWithSettings(
      {
        enabledLevels: levels.info,
        level: 'info',
        color: false,
      },
      'Test!',
    );
    expect(spyInfo).toHaveBeenCalledWith('Test!');

    spyInfo.mockClear();
    logger.info('Test!');
    expect(spyInfo).not.toHaveBeenCalled();
  });
});
