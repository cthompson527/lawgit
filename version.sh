#! /usr/bin/env bash

set -uxo pipefail

git diff --quiet --exit-code
if [ $? -ne 0 ]; then
  echo 'You have unstaged changes. You need to run in a clean directory.'
  exit 1
fi

git diff --quiet --exit-code --cached
if [ $? -ne 0 ]; then
  echo 'You have uncommitted changes. You need to run in a clean directory.'
  exit 1
fi

branch=$(git symbolic-ref --short HEAD)
if [[ $branch == "master" ]]; then
  echo 'You cannot run this script on the master branch.'
  exit 1
fi

set -e

semver_bump_type=$1
shift

set +x

echo 'You are about to:'
echo "  1.) Bump the package version as a $semver_bump_type"
echo '  2.) git commit --amend the last commit with the new version and force push'
echo '  3.) git tag the new version and push'
echo '  4.) Deploy the library to the npm registry'

read -r -p "Are you sure you want to continue? [y/N] " response
if [[ ! "$response" =~ ^([yY][eE][sS]|[yY])$ ]]; then
  exit 0
fi

set -x

new_version=$(npm version --no-git-tag-version $semver_bump_type)

git add -A

commits_since_master=$(git rev-list --count master...$branch)
if [ $commits_since_master -eq 0 ]; then
  git commit -m ":arrow_up: Bump to version $new_version"
  git push origin $branch
elif [ $commits_since_master -eq 1 ]; then
  git commit --amend --no-edit
  git push origin $branch -f
else
  echo 'You have too many commits in your branch. Squash your commits, create a good commit message, and try again.'
  exit 1
fi

git tag "v$new_version"
git push --tags
