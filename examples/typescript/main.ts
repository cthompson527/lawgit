import Lawgit from 'lawgit';
const logger = Lawgit();

const { levels } = logger;

logger.log('This is the logger.');
logger.info('Logging in the default settings will use the "info" level');
logger.debug('However, you can log in the debug level too.');
logger.warn('But, I must warn you...');
logger.error(
  'Attempting to log anything other than a string will result in an error',
);

logger.info('But, you can also call JSON.stringify:');
logger.debug(`${JSON.stringify(logger.globalSettings(), null, '\t')}`);

logger.info(
  'You can change the default settings to remove some enabled levels.',
);
logger.setGlobalSettings({ enabledLevels: levels.warn | levels.error });
logger.warn('But, I have to warn you, if you turn off the wrong level,');
logger.error('You');
logger.debug('might'); // not shown
logger.error('miss');
logger.debug('important'); // not shown
logger.error('information');
