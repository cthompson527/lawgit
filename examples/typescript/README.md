# Examples Typescript
Lawgit example using Typescript

## Installation

Run `npm install` and the required packages should be installed.

## Running

Run `npx ts-node main.ts`
